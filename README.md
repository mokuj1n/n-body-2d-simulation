# *N-Body 2D Simulation*

A simple **n-body** simulation using Newton's law of universal gravitation.

## How to run
1. Install pygame module: `pip install pygame` or `pip install -r requirements.txt`
2. Run: `python -m nbody`
