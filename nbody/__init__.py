import os

__author__ = "Vadim Trifonov"
__copyright__ = "Copyright 2022 Vadim Trifonov"
__license__ = "MIT"
__maintainer__ = __author__
__email__ = "retroarefobia@gmail.com"
__version__ = "0.2.1"
__version_info__ = tuple(int(part) for part in __version__.split('.'))
__description__ = "A simple **n-body** simulation using Newton's law of universal gravitation"


os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
