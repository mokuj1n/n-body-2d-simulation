from nbody.colors import Color
import nbody.constance as c
from pygame.math import Vector2
from pygame import gfxdraw


class Body:

    def __init__(self, position, mass, color, velocity):
        self.position = position
        self.mass = mass
        self.color = color
        self.velocity = velocity
        self.radius = max(int(mass ** (1/3)), 1)
        self.force = Vector2()

    def update_force(self, bodies):
        """ Apply gravitational forces """
        force = Vector2()
        for body in bodies:
            if not body is self:
                direction = body.position - self.position
                r = direction.magnitude_squared()
                force += c.G * (self.mass * body.mass / r) * direction
        self.force = force

    def update_velocity(self):
        """ Apply force and update velocity """
        self.velocity += self.force / self.mass

    def update_position(self):
        """ Apply velocity and update position """
        self.position += self.velocity

    def merging(self, bodies):
        for body in bodies:
            if not body is self:
                distance = self.position.distance_to(body.position)
                if distance < self.radius:
                    ratio = self.mass / (self.mass + body.mass)
                    self.velocity = ratio * self.velocity + \
                        (1 - ratio) * body.velocity
                    self.mass += body.mass
                    self.radius = max(int(self.mass ** (1/3)), 1)
                    bodies.remove(body)

    def draw_vectors(self, screen, panning_offset, scale_factor=4):
        # Draw debug force vector
        gfxdraw.line(screen,
                     int(self.position.x + panning_offset[0]),
                     int(self.position.y + panning_offset[1]),
                     int(self.position.x +
                         panning_offset[0] + self.force.x * scale_factor / 4),
                     int(self.position.y +
                         panning_offset[1] + self.force.y * scale_factor / 4),
                     Color.RED)

        # Draw debug velocity vector
        gfxdraw.line(screen,
                     int(self.position.x + panning_offset[0]),
                     int(self.position.y + panning_offset[1]),
                     int(self.position.x +
                         panning_offset[0] + self.velocity.x * scale_factor),
                     int(self.position.y +
                         panning_offset[1] + self.velocity.y * scale_factor),
                     Color.GREEN)

    def draw(self, screen):
        return NotImplementedError


class Planet(Body):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def draw(self, screen, panning_offset):
        gfxdraw.aacircle(screen, int(self.position.x + panning_offset[0]), int(
            self.position.y + panning_offset[1]), self.radius, self.color)

        gfxdraw.filled_circle(screen, int(self.position.x + panning_offset[0]), int(
            self.position.y + panning_offset[1]), self.radius, self.color)
