import pygame as pg
from pygame.math import Vector2
import sys
import numpy as np
from argparse import ArgumentParser
from operator import attrgetter

import nbody.constance as c
import nbody.simulation as s
from nbody import __version__
from nbody.colors import Color


class Application:

    def __init__(self):
        pg.init()
        pg.font.init()
        pg.display.set_caption(f"N-Body Simulation {__version__}")

        infoObject = pg.display.Info()
        c.DEVICE_RESOLUTION = (infoObject.current_w, infoObject.current_h)

        self.screen = pg.display.set_mode(c.APP_RESOLUTION)
        self.screen.fill(Color.BACKGROUND)

        self.font = pg.font.SysFont('monospace', 16)
        self.clock = pg.time.Clock()

        self.running = True
        self.drawing = False
        self.body_folowing = False

        self.panning = False
        self.panning_offset = [0, 0]
        self.fullscreen = False

    def main_loop(self, simulation):

        while self.running:

            simulation.update_bodies()

            self.screen.fill(Color.BACKGROUND)
            simulation.draw(self.screen, self.panning_offset)

            if self.body_folowing:
                self.massive_body_folowing(simulation)

            for event in pg.event.get():
                if event.type == pg.QUIT:
                    self.running = False

                if event.type == pg.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.drawing = True
                        self.mouse_screen_drawing(simulation)
                    if event.button == 3:
                        self.panning = True
                        self.mouse_screen_panning(simulation)

                    if event.button == 4:
                        # Mouse wheel up
                        c.G += 0.000025

                    if event.button == 5:
                        # Mouse wheel down
                        c.G -= 0.000025

                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_SPACE:
                        self.body_folowing = not self.body_folowing
                    if event.key == pg.K_F11:
                        self.fullscreen = not self.fullscreen
                        if self.fullscreen:
                            self.screen = pg.display.set_mode(
                                c.DEVICE_RESOLUTION, pg.FULLSCREEN)
                            self.panning_offset = [
                                (c.DEVICE_RESOLUTION[0] - c.APP_RESOLUTION[0]) / 2, (c.DEVICE_RESOLUTION[1] - c.APP_RESOLUTION[1]) / 2]

                        else:
                            self.panning_offset = [0, 0]
                            self.screen = pg.display.set_mode(c.APP_RESOLUTION)

            displayed_stats = [
                f"FPS: {int(self.clock.get_fps())}",
                f"BODIES: {len(simulation.bodies)}",
                f"G-CONST: {c.G:.5f}"
            ]

            self.display_text_lines(
                displayed_stats, pos=(10, 10), color=Color.WHITE)

            self.clock.tick(c.FRAMERATE)
            pg.display.flip()

        pg.quit()

    def display_text(self, text, pos: tuple, color=Color.BLACK):
        textblock = self.font.render(text, True, color)
        self.screen.blit(textblock, pos)

    def display_text_lines(self, lines: list, pos: tuple, color=Color.BLACK):
        y = pos[1]
        for line in lines:
            self.display_text(line, (pos[0], y), color)
            y += 15

    def massive_body_folowing(self, sim):
        """ Move the camera behind the most massive object in the scene """

        massive_body = max(sim.bodies, key=attrgetter('mass'))

        if not massive_body:
            return

        w, h = self.screen.get_size()
        self.panning_offset[0] = int(w / 2) - massive_body.position.x
        self.panning_offset[1] = int(h / 2) - massive_body.position.y

    def mouse_screen_drawing(self, sim):
        """ Allows you to add new bodies by pressing the left mouse button """

        duration = 1
        start_pos = Vector2(pg.mouse.get_pos())

        while self.drawing:
            end_pos = Vector2(pg.mouse.get_pos())
            vel = Vector2(start_pos.x - end_pos.x,
                          start_pos.y - end_pos.y) * 0.03
            direction = start_pos + vel * 10

            self.screen.fill(Color.BACKGROUND)
            pg.gfxdraw.aacircle(self.screen, int(start_pos.x),
                                int(start_pos.y), int(duration), Color.WHITE)
            pg.gfxdraw.filled_circle(self.screen, int(start_pos.x),
                                     int(start_pos.y), int(duration), Color.WHITE)

            pg.gfxdraw.line(self.screen,  int(start_pos.x), int(
                start_pos.y), int(direction.x), int(direction.y), Color.WHITE)
            self.draw_line_dashed(self.screen,  Color.WHITE, (int(start_pos.x),
                                                              int(start_pos.y)), (int(end_pos.x), int(end_pos.y)))

            sim.draw(self.screen, self.panning_offset)
            pg.display.flip()

            color = Color.random_bright_color()

            for event in pg.event.get():
                if event.type == pg.MOUSEBUTTONUP:
                    self.drawing = False
                    sim.new_body(start_pos, vel, color,
                                 duration, self.panning_offset)

            duration += 0.03

    def mouse_screen_panning(self, sim):
        """ Allows you to move the scene when holding down the right mouse button """

        initial_offset = self.panning_offset.copy()
        pan_start = pg.mouse.get_pos()

        while self.panning:
            pan_new = pg.mouse.get_pos()
            self.panning_offset[0] = initial_offset[0] + \
                pan_new[0] - pan_start[0]
            self.panning_offset[1] = initial_offset[1] + \
                pan_new[1] - pan_start[1]
            self.screen.fill(Color.BACKGROUND)
            sim.draw(self.screen, self.panning_offset)
            pg.display.flip()

            for event in pg.event.get():
                if event.type == pg.MOUSEBUTTONUP:
                    self.panning = False

    def draw_line_dashed(self, surface, color, start_pos, end_pos, width=1, dash_length=4, exclude_corners=True):
        # convert tuples to numpy arrays
        start_pos = np.array(start_pos)
        end_pos = np.array(end_pos)

        # get euclidian distance between start_pos and end_pos
        length = np.linalg.norm(end_pos - start_pos)

        # get amount of pieces that line will be split up in (half of it are amount of dashes)
        dash_amount = int(length / dash_length)

        # x-y-value-pairs of where dashes start (and on next, will end)
        dash_knots = np.array([np.linspace(
            start_pos[i], end_pos[i], dash_amount) for i in range(2)]).transpose()

        return [pg.draw.line(surface, color, tuple(dash_knots[n]), tuple(dash_knots[n+1]), width)
                for n in range(int(exclude_corners), dash_amount - int(exclude_corners), 2)]


def main(args):

    app = Application()

    if args.sim == 'solar':
        sim = s.SolarSystem(
            merging=args.disable_merging,
            debug=args.debug)
        sim.generate_bodies(p_count=args.num_of_bodies)

    elif args.sim == 'stable':
        sim = s.StableOrbit(
            merging=args.disable_merging,
            debug=args.debug)
        sim.generate_bodies()

    elif args.sim == 'empty':
        sim = s.Empty(
            merging=args.disable_merging,
            debug=args.debug)

    app.main_loop(sim)


if __name__ == "__main__":
    parser = ArgumentParser(prog='nbody')
    parser.add_argument('-s', '--sim',
                        choices=['solar', 'stable', 'empty'], default='solar')
    parser.add_argument('-n', '--num_of_bodies',
                        type=int, default=50)
    parser.add_argument('--disable-merging', action='store_false')
    parser.add_argument('--debug', action='store_true')

    sys.exit(main(parser.parse_args()))
