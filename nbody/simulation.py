import random as r
import math

from pygame.math import Vector2

import nbody.constance as c
from nbody.bodies import Planet
from nbody.colors import Color


class Simulation:

    def __init__(self, merging, debug):
        self.bodies = []
        self.merging = merging
        self.debug = debug

    def generate_bodies(self):
        return NotImplementedError

    def update_bodies(self):
        for body in self.bodies:
            body.update_force(self.bodies)
            body.update_velocity()
            body.update_position()
            if self.merging:
                body.merging(self.bodies)

    def new_body(self, start_pos, vel, color, duration, panning_offset):
        pos = Vector2(start_pos.x - panning_offset[0],
                      start_pos.y - panning_offset[1])

        mass = duration ** 3
        p = Planet(pos, mass, color, vel)

        self.bodies.append(p)

    def draw(self, screen, panning_offset):
        for body in self.bodies:
            body.draw(screen, panning_offset)
            if self.debug:
                body.draw_vectors(screen, panning_offset)


class Empty(Simulation):

    def __init__(self, merging, debug):
        super(Empty, self).__init__(merging, debug)


class SolarSystem(Simulation):

    Iv = 3.8
    BodyMassMin = 10
    BodyMassMax = 100

    def __init__(self, merging, debug):
        super(SolarSystem, self).__init__(merging, debug)

    def generate_bodies(self, p_count):
        _center = Vector2(int(c.APP_WIDTH / 2), int(c.APP_HEIGHT / 2))

        sun = Planet(position=_center, mass=50000,
                     color=Color.RED, velocity=Vector2())

        self.bodies.append(sun)

        for i in range(p_count):
            r_pos = Vector2(r.randint(0, c.APP_WIDTH),
                            r.randint(0, c.APP_HEIGHT))
            r_mass = r.randint(self.BodyMassMin, self.BodyMassMax)

            direction = _center - r_pos
            distance = direction.magnitude()
            sun_direction = direction.normalize()

            vel = Vector2(sun_direction.y, -sun_direction.x)

            cm = self.bodies[0].mass + r_mass
            velocity = self.Iv * vel * \
                math.sqrt(c.G * cm / math.sqrt(distance))
            color = Color.random_bright_color()

            self.bodies.append(Planet(r_pos, r_mass, color, velocity))


class StableOrbit(Simulation):

    def __init__(self, merging, debug):
        super(StableOrbit, self).__init__(merging, debug)

    def generate_bodies(self):
        pass
