
from random import sample


class Color:
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)

    CHARCOAL = (4, 16, 21)
    PERSIAN_GREEN = (42, 157, 143)
    ASPARAGUS = (138, 177, 125)
    MAIZE_CRAYOIA = (233, 196, 106)
    EARTH_YELLOW = (239, 179, 102)
    SANDY_BROWN = (244, 162, 97)
    BURNT_SIENNA = (231, 111, 81)
    SMOKY_BLACK = (17, 17, 17)

    BACKGROUND = (17, 17, 17)

    @staticmethod
    def random_bright_color():
        while True:
            R, G, B = sample(range(0, 256), 3)
            diff_sum = abs(R-G) + abs(G-B) + abs(R-B)
            if diff_sum > 240:
                return (R, G, B)

    @staticmethod
    def random_dim_color():
        while True:
            R, G, B = sample(range(0, 256), 3)
            diff_sum = abs(R-G) + abs(G-B) + abs(R-B)
            totl_sum = R + G + B
            if diff_sum > 80 and diff_sum < 200 and totl_sum > 200 and totl_sum < 500:
                return (R, G, B)
